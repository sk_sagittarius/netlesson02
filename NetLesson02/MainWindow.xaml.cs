﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace NetLesson02
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread srvThread; // поток сервера
        TcpListener srvSocket; // сокет сервера
        bool isStopServer; // флаг, останавливающий сервер

        public MainWindow()
        {
            InitializeComponent();
            cbIpServer.Items.Add("0.0.0.0");
            cbIpServer.Items.Add("127.0.0.1");
            foreach (var a in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            {
                cbIpServer.Items.Add(a.ToString());
            }
            cbIpServer.SelectedIndex = 0;
            // флаг  - запущен сервер или нет
            btnStart.Tag = false; // доп структура к кнопке. вместо проверки bool isStart
            //Dns.GetHostEntry(Dns.GetHostName());
            isStopServer = false;  // флаг, останавливающий сервер
        }


        private void BtnStartClick(object sender, RoutedEventArgs e)
        {
            if (false == (bool)btnStart.Tag)
            { // запускаем сервер
                //cbIpServer.SelectedItem.ToString() // ip address
                srvSocket = new TcpListener(IPAddress.Parse(cbIpServer.SelectedItem.ToString()), int.Parse(txtPort.Text)); // создание сокета сервера для прослушки
                srvSocket.Start(100); // запуск прослушивания сети по Ip:port
                // далее должна  быть команда Accept()
                srvThread = new Thread(ServerThreadRoutine); // создаем поток
                //srvThread.IsBackground = true; // сервер умрет вместе с потоком
                srvThread.Start(srvSocket);

                btnStart.Content = "Stop";
                btnStart.Tag = true;
            }
            else
            { // останавливаем сервер
                btnStart.Content = "Start";
                btnStart.Tag = false;
            }
        }

        void ServerThreadRoutine(object obj)
        {
            //ThreadPool.SetMaxThreads(); // подготовка пула потоков 
            TcpListener srvSocket = obj as TcpListener;

            #region синхронный вариант сервера
            //while (true)
            //{
            //    TcpClient client = srvSocket.AcceptTcpClient(); // блокирующая функция, не асинхронная. ожидание покдлючения удаленного клиента
            //    // запуск клиентского потока - работа с клиентом в отдельном потоке
            //    ThreadPool.QueueUserWorkItem(ClientThreadRoutine, client); // Pool быстрее чем new Thread
            //}
            #endregion

            #region ассинхронный вариант Accept()
            while (true)
            {
                IAsyncResult ia = srvSocket.BeginAcceptTcpClient(ClientThreadRoutine, srvSocket);

                // ожидание завершения асинхронного вызова
                while (ia.AsyncWaitHandle.WaitOne(200) == false) // 200 - время ожидания клиента, чтобы не застрять в цикле 
                { // WaitOne завершился по timeout'у
                    if (isStopServer == true)
                    { // завершение приема удаленных клиентов
                        //srvSocket.EndAcceptSocket(ia); // ???
                        return;
                    }
                }
            }
            #endregion
        }

        void ClientThreadRoutine(IAsyncResult ia)
        {
            TcpListener srvSocket = ia.AsyncState as TcpListener;
            TcpClient client = srvSocket.EndAcceptTcpClient(ia);
            ThreadPool.QueueUserWorkItem(ClientThreadRoutine2, client);
        }

        #region другой способ передачи текста в текст бокс
        delegate void DelegateAppendTextBox(TextBox textBox, string str);
        void AppendTextBox(TextBox textBox, string str)
        {
            textBox.AppendText(str + "\n");
        }
        #endregion

        void ClientThreadRoutine2(object obj)
        { // клиентский поток, поток работы с удаленным клиентом
            TcpClient client = obj as TcpClient;
            // вывод информации в журнал о соединении
            Dispatcher.Invoke(() =>
            {
                txtLog.AppendText("Успешное соединение клиента\n");
            });
            txtLog.Text += "Успешное соединение клиента\r\n";

            #region другой способ передачи текста в текст бокс
            string str = "IP:Port клиента: " + client.Client.RemoteEndPoint.ToString(); // инфо о клиенте, айпи и порт
            Dispatcher.Invoke(new DelegateAppendTextBox(AppendTextBox), txtLog, str);
            // TODO отправка сообщения в клиент
            #endregion

            // дальше про протоколу
            byte[] buf = new byte[4 * 1024];
            // 1- ждем от клиента имя (логн, пароль, аутентификация)
            int recSize = client.Client.Receive(buf);
            Dispatcher.Invoke(new DelegateAppendTextBox(AppendTextBox), txtLog, "Имя клиента: " + Encoding.UTF8.GetString(buf, 0, recSize));
            // 2 - ответ сервера
            string clientName = Encoding.UTF8.GetString(buf, 0, recSize);
            client.Client.Send(Encoding.UTF8.GetBytes("Hello " + clientName + "!"));

            while(true)
            {
                recSize = client.Client.Receive(buf);
                Dispatcher.Invoke(new DelegateAppendTextBox(AppendTextBox), txtLog, Encoding.UTF8.GetString(buf, 0, recSize));
                client.Client.Send(buf, recSize, SocketFlags.None);
            }
        }
    }
}
